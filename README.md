# Hi, I'm Kilian 👋

I'm a passionate DevOps Engineer with an interest in Cloud ☁️ and container 🐋 technologies. 

## Some of my Projects 💻

- [Excalidraw-fork](https://gitlab.com/kiliandeca/excalidraw-fork): A fork of Excalidraw suitable for self-hosting, I added dynamic environment variable configuration at runtime and support for alternative storage backend.

- [Minekloud](https://gitlab.com/cpelyon/5irc-minekloud): Tech Lead for a student project where we built an entire hosting platform for multiplayer gaming servers. Every workload was containerized and hosted on a GCP K8S cluster.

- [DecaMiner](https://youtu.be/NZQL2_i2Z3w?t=132): One of my first projects where I tinkered with programable robots to mine for me, it's funny to see that I was already focusing on automating repetitive tasks back then!

## Open source contributions 🚀

- [GitLab](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/25758): Edited the official documentation about npm dependencies caching in CI to follow good practices.
- [Nest.js](https://github.com/nestjs/nest/pull/5291): Worked on a new feature for the framework's requests routing system.
- [class-validator](https://github.com/typestack/class-validator/pulls?q=author%3Akiliandeca): Added multiple validation decorators

Check out [to-be-continuous](https://gitlab.com/to-be-continuous), I made some contributions when the project was still Intra Source at Orange🍊

## I also tried to write 📝

- [We built a Minecraft protocol reverse proxy](https://dev.to/kiliandeca/we-built-a-minecraft-protocol-reverse-proxy-2e4f)
